# 小程序商城

> 小程序的前13章都看完了,但是还是感觉前端部分不熟练,现在再重头写一遍,

## banner接口

    没有什么要写的,校验参数,关联模型,获取数据即可

    getBannerList
    
## theme接口

    进行id集合遍历时通过foreach依次校验元素是否为正整数,产品和专题之间是多对多的关系
    
    getThemeList 获取专题列表

    getThemeDetail 获取专题详情

## product接口

    getRecentProduct 获取最近商品

    getProductDetail 获取商品详情

    getProductByCategoryID 获取分类中的商品

## category接口

    getCategoryAll 获取全部分类

## token接口

> 当访问一个需要权限的接口是需要用到token校验
 
> token信息保存在缓存中,具有时效性

* user_id 用户id
* scope 用户权限
* wxResult 微信返回信息

> 流程: 获取code码 - 组装微信获取信息地址 - 判断信息 - 通过openid创建用户 - 将创建的用户id及权限和微信信息保存在缓存中 - 将随机码作为Key值返回给客户端

## address接口

    createOrUpdate 新增或更新地址

    通过hasOne关联address判断更新还是新增

    此处增加前置校验权限方法

## order接口

    下单接口思路

        将商品id及数量传递到服务器

        服务器根据Id将商品详细信息获取,根据用户要购买的商品数量进行数据库查询判断库存量

        返回商品信息并组成订单信息

        将订单的必要信息保存为订单快照并写入字段

        返回订单状态

        订单快照包括 订单图片 订单地址 订单名称 订单商品信息

    getOrderDetail 获取订单详情

    getSummaryUser 获取用户历史订单

        用户的历史订单每次只展示15条,根据下拉操作触发获取请求,通过当前索引位置发送并返回下面的数据

        需要获取用户订单数据以及当前索引位置

## pay接口

    支付接口思路

        传递订单id,配置wx所需类库

        校验订单库存

        创建预订单

            设置参数 wxPayUnifiedOrder

                openid

                订单号

                支付类型

                项目描述

                回调地址

                金额
            
            创建

                传递设置的参数以及配置

                将返回的prepay_id返回并插入

            获取签名

                将返回的数据以及配置传递

                appid

                时间戳

                随机字符串

                prepay_id

        返回参数给微信小程序调起支付

    回调接口思路

        检测库存量

        更新订单状态

        更新库存

        已知BUG:HOST_POST_DATA在7.0不支持 - 改为file_get_contents('php://input')

        有一些细节确实需要注意
    
