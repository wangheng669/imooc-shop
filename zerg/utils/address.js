import { Base } from 'base.js';

var base = new Base();

class Address extends Base {

  constructor() {
    super();
  }

  // 设置地址
  setAddressInfo(res){
    var province = res.provinceName || res.province,
      city = res.cityName || res.city,
      country = res.countyName || res.country,
      detail = res.detailInfo || res.detail;

    var totalDetail = city + country + detail;
    if(!this.isCenterCity(province)){
      totalDetail = province + totalDetail;
    }
    return totalDetail;
  }

  // 是否为直辖市
  isCenterCity(name){
    var centerCitys = ['北京市','天津市','上海市','重庆市'],
      flag = centerCitys.indexOf(name) >=0;
    return flag;
  }

  // 更新地址
  submitAddress(res,callback){
    var data = this._setUpAddress(res);
    var params = {
      url: 'address',
      method: 'POST',
      data: data,
      sCallback:function(res){
        callback && callback(true,res)
      },eCallback(res){
        callback && callback(false,res);
      }
    };
    this.request(params);
  }

  // 保存地址
  _setUpAddress(res){
    var formData = {
      name: res.userName,
      province : res.provinceName,
      city: res.cityName,
      country: res.countyName,
      mobile : res.telNumber,
      detail: res.detailInfo,
    }
    return formData;
  }

  // 获取地址
  getAddress(callback){
    var that = this;
    var params = {
      url: 'address/user',
      method: 'GET',
      sCallBack: function (res) {
        res.totalDetail = that.setAddressInfo(res);
        callback && callback(res);
      }
    };
    this.request(params);
  }

}

export { Address }