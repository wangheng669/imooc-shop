import { Base } from '../../utils/base.js';

class Theme extends Base {

  constructor() {
    super();
  }
  // theme接口
  getProductsData(id, callback) {
    var params = {
      url: 'theme/' + id,
      method: 'GET',
      sCallBack: function (res) {
        callback && callback(res);
      }
    };
    this.request(params);
  }
}
export { Theme };