
import {Theme} from '../../pages/theme/theme-modle.js';

var theme = new Theme();

Page({
  data: {

  },
  onLoad: function (options) {
    var id = options.id;
    var name = options.name;
    this.data.id = id;
    this.data.name = name;
    this._loadDta();
  },
  onReady: function(){
    wx.setNavigationBarTitle({
      title: this.data.name,
    })
  },
  _loadDta: function (){
    theme.getProductsData(this.data.id, (res) => {
      console.log(res);
      this.setData({
        'themeInfo': res,
      });
    });
  },
  onProductItemTap: function (event){
    var id = theme.getDataSet(event, 'id');
    var name = theme.getDataSet(event, 'name');
    wx.navigateTo({
      url: '../product/product?id=' + id + '&name=' + name,
    })
  }
})