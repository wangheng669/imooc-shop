
import { My } from '../../pages/my/my-model.js';
import { Order } from '../../pages/order/order-model.js';
import { Address } from '../../utils/address.js';

var my = new My();

var order = new Order();

var address = new Address();

Page({
  data: {
    pageIndex: 1,
    isLoadedAll: false,
    orderArr: [],
  },
  onLoad: function (){
    this._loadData();
    this._getAddressInfo();
  },
  onShow: function (){
    var newOrderFlag = order.hasNewOrder();
    if (newOrderFlag){
      this.refresh();
    }
  },

  // 重新获取订单列表
  refresh: function (){
    var that = this;
    this.data.orderArr = [];
    this._getOrderList(()=>{
      that.data.isLoadedAll = false;
      that.data.pageIndex = 1;
      order.execSetStorageSync(false);  //更新标志位      
    });
  },

  _loadData: function(){
    // 获取用户信息
    my.getUserInfo((data)=>{
      this.setData({
        userInfo: data,
      })
    })
    this._getOrderList();
    order.execSetStorageSync(false);  //更新标志位
    
  },

  // 获取用户地址
  _getAddressInfo:function(){

    address.getAddress((addressInfo) =>{
      this._bindAddress(addressInfo);
    });
  },

  // 获取用户订单
  _getOrderList: function (callback){
    var that = this;
    order.getOrders(that.data.pageIndex,(res) => {
      var data = res.data;
      if(data){
        this.data.orderArr.push.apply(this.data.orderArr, data)
        this.setData({
          orderArr: this.data.orderArr
        });
      }else{
        that.data.isLoadedAll = true;
        that.data.pageIndex = 1;
      }
      callback && callback();
    });
  },

  // 绑定地址信息
  _bindAddress: function (addressInfo){
    this.setData({
      addressInfo: addressInfo,
    });
  },

  // 上滑加载更多历史订单
  onReachBottom: function(){
    if (!this.data.isLoadAll){
      this.data.pageIndex++;      
      this._getOrderList();      
    }
  },

  // 显示订单的具体信息
  showOrderDetailInfo: function (event){
    var id = order.getDataSet(event,'id');
    wx.navigateTo({
      url: '../order/order?from=order&id=' + id,
    })
  },

  // 订单付款
  rePay: function (event){
    var id = order.getDataSet(event,'id');
    this._execPay(id);
  },

   _execPay: function (id) {
    var that = this;
    order.execPay(id, (statusCode) => {
      if (statusCode != 0) {
        var flag = statusCode == 2;
        wx.navigateTo({
          url: '../pay-result/pay-result?id=' + id + '&flag=' + flag + '&from=my',
        })
      }else{
        that.showTips('支付失败','商品已下架或库存不足');
      }
    });
  },

  // 提示窗口
  showTips: function (title, content, flag) {
    wx.showModal({
      title: title,
      content: content,
      showCancel: false,
      success: function (res) {
        if (flag) {
          wx.switchTab({
            url: '/pages/my/my',
          })
        }
      }
    });
  },

  // 更改收货地址
  editAddress: function (event) {
    var that = this;
    wx.chooseAddress({
      success: function (res) {
        var addressInfo = {
          name: res.userName,
          mobile: res.telNumber,
          totalDetail: address.setAddressInfo(res),
        }
        that._bindAddressInfo(addressInfo);

        // 保存地址
        address.submitAddress(res, (flag) => {
          if (!flag) {
            that.showTips('操作提示', '地址信息更新失败');
          }
        });
      }
    })
  },

})