import {Base} from '../../utils/base.js';

class Home extends Base{

  constructor(){
    super();
  }
  // banner接口
  getBannerData(id,callback){
    var params = {
      url: 'banner/' + id,
      method: 'GET',
      sCallBack: function (res) {
        callback&&callback(res.items);
      }
    };
    this.request(params);
  }
  // theme接口
  getThemeData(ids, callback) {
    var params = {
      url: 'theme?ids=' + ids,
      method: 'GET',
      sCallBack: function (res) {
        callback && callback(res);
      }
    };
    this.request(params);
  }
  // 最近商品接口
  getProductData(count, callback) {
    var params = {
      url: 'product?count=' + count,
      method: 'GET',
      sCallBack: function (res) {
        callback && callback(res);
      }
    };
    this.request(params);
  }
}
export {Home};