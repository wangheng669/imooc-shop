
import {Home} from 'home-model.js';
var home = new Home();

Page({
  data: {

  },
  onLoad: function () {
    this._loadData();
  },

  // 初始化时调用
  _loadData: function(){
    var id = 1;
    home.getBannerData(id,(res)=>{
      this.setData({
        'bannerArr':res,
      });
    });
    var ids = '1,2,3';
    home.getThemeData(ids,(res)=>{
      this.setData({
        'themeArr':res,
      });
    });
    var count = '15';
    home.getProductData(count, (res) => {
      this.setData({
        'productArr': res,
      });
    });
  },

  // banner页面跳转
  onProductsItemTap:function(event){
    var id = home.getDataSet(event,'id');
    var name = "test";
    wx.navigateTo({
      url: '../product/product?id=' + id + "&name=" + name,
    })
  },
  // theme页面跳转
  onThemeItemTap: function (event) {
    var id = home.getDataSet(event, 'id');
    var name = home.getDataSet(event, 'name');
    wx.navigateTo({
      url: '../theme/theme?id=' + id +"&name=" + name,
    })
  },
  // 产品页面跳转
  onProductItemTap: function (event) {
    var id = home.getDataSet(event, 'id');
    var name = home.getDataSet(event, 'name');
    wx.navigateTo({
      url: '../product/product?id=' + id +'&name=' + name,
    })
  },
  



})