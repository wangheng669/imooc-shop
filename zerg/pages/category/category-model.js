import { Base } from '../../utils/base.js';

class Category extends Base {

  constructor() {
    super();
  }
  // 分类类型接口
  getCategoryType(callback) {
    var params = {
      url: 'category',
      method: 'GET',
      sCallBack: function (res) {
        callback && callback(res);
      }
    };
    this.request(params);
  }
  getProductByCategory(id, callback){
    var params = {
      url: 'product/category/' + id,
      method: 'GET',
      sCallBack: function (res) {
        callback && callback(res);
      }
    };
    this.request(params);
  }
}
export { Category };