
import { Cart } from '../../pages/cart/cart-model.js';

var cart = new Cart();

Page({
  data: {
  },
  onHide: function(){
    wx.setStorageSync(cart._storageKeyName, this.data.cartData);
  },
  onLoad: function (options) {

  },
  onShow: function () {
    // 获取购物车内的商品
    var cartData = cart.getCartDataFromLocal();
    // 获取选种商品信息
    var cal = this._calcTotalAccountAndCounts(cartData);
    this.setData({
      selectedCounts: cal.selectedCounts,
      account: cal.account,
      selectedTypeCounts: cal.selectedTypeCounts,
      cartData: cartData,
    });
  },
  _calcTotalAccountAndCounts:function(data){
    var len = data.length,
    
    // 选中商品的总价格
    account = 0,
    
    // 购买商品的总个数
    selectedCounts = 0,
    
    // 购买商品种类的总数
    selectedTypeCounts = 0;
    
    let multiple = 100;

    for(let i=0;i<len;i++){
      if(data[i].selectStatus){
        selectedCounts += data[i].counts;
        account += data[i].counts * multiple * Number(data[i].price) * multiple;
        selectedTypeCounts++;
      }
    }
  
    return {
      selectedCounts: selectedCounts,
      selectedTypeCounts: selectedTypeCounts,
      account: account / (multiple * multiple),
    };

  },
  // 单选操作
  toggleSelect:function(event){
    var id = cart.getDataSet(event, 'id');
    var status = cart.getDataSet(event, 'status');
    // 根据id获取当前元素下标
    var index = this._getProductIndexById(id);
    this.data.cartData[index].selectStatus = !status;
    this._resetCarData();
  },
  // 全选操作
  toggleSelectAll:function(event){
    var newData = this.data.cartData,
      len = newData.length;
    var status = cart.getDataSet(event, 'status') == 'true';
    for (var i = 0; i < len;i++){
      newData[i].selectStatus = !status;
    }
    this._resetCarData();
  },
  // 根据id获取元素下标
  _getProductIndexById:function(id){
    var data = this.data.cartData,
        len = data.length;
    for(let i=0;i<len;i++){
      if(data[i].id == id){
        return i;
      }
    }
  },
  // 重新计算商品价格
  _resetCarData:function(){
    var newData = this._calcTotalAccountAndCounts(this.data.cartData);
    this.setData({
      selectedCounts: newData.selectedCounts,
      account: newData.account,
      selectedTypeCounts: newData.selectedTypeCounts,
      cartData: this.data.cartData,
    });
  },
  // 更改商品的数量
  changeCounts:function(event){
    var type = cart.getDataSet(event,'type');
    var id = cart.getDataSet(event,'id'),
    index = this._getProductIndexById(id),
    counts = 1;
    if(type == 'add'){
      cart.addCounts(id);
    }else{
      counts = -1;
      cart.cutCounts(id);
    }
    if (this.data.cartData[index].counts > 1){
      this.data.cartData[index].counts += counts;
    }
    this._resetCarData();
  },
  // 删除购物车商品
  deleteCartData:function(event){
    var id = cart.getDataSet(event, 'id'),
      index = this._getProductIndexById(id);
    this.data.cartData.splice(index,1);
    this._resetCarData();
    cart._deleteCartData(id);
  },
  // 提交购物车商品
  submitOrder:function(){
    wx.navigateTo({
      url: '../order/order?account=' + this.data.account + '&from=cart',
    })
  }
})