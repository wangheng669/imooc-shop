import { Base } from '../../utils/base.js';

class Product extends Base {

  constructor() {
    super();
  }
  // 商品详情接口
  getProductData(id, callback) {
    var params = {
      url: 'product/' + id,
      method: 'GET',
      sCallBack: function (res) {
        callback && callback(res);
      }
    };
    this.request(params);
  }
}
export { Product };