<?php

use think\Route;

Route::get('banner/:id','api/v1.Banner/getBannerList');
Route::get('theme/:id','api/v1.Theme/getThemeDetail',[],['id'=>'\d+']);
Route::get('theme','api/v1.Theme/getThemeList');
Route::get('product/:id','api/v1.Product/getProductDetail',[],['id'=>'\d+']);
Route::get('product','api/v1.Product/getRecentProduct');
Route::get('product/category/:id','api/v1.Product/getProductInCategory',[],['id'=>'\d+']);
Route::get('category','api/v1.Category/getCategoryAll');
Route::post('token/user','api/v1.Token/getToken');
Route::post('address','api/v1.Address/createOrUpdate');
Route::get('address/user','api/v1.Address/getAddress');
Route::post('order/:id','api/v1.Order/orderDetail');
Route::post('order/place','api/v1.Order/placeOrder');
Route::get('order/by_user','api/v1.Order/getSummaryUser');
Route::post('pay/re_notify','api/v1.Pay/receiveNotify');
Route::post('pay','api/v1.Pay/preOrder');
Route::post('pay/notify','api/v1.Pay/notify');
