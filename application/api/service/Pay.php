<?php

namespace app\api\service;

use app\api\model\Order as OrderModel;
use app\api\service\Order as OrderService;
use app\api\service\Token as TokenService;
use app\lib\exception\OrderException;
use app\lib\enum\OrderStatusEnum;
use think\Loader;
use think\Exception;

Loader::import('WxPay.WxPay',EXTEND_PATH,'.Api.php');

class Pay{

    protected $orderID;
    protected $orderNo;

    public function __construct($orderID)
    {
        $this->orderID = $orderID;
    }

    /* 支付主方法 */
    public function pay()
    {
        /* 校验订单正确性 */
        $this->checkOrderValid();
        /* 检测库存量 */
        $orderService = new OrderService();
        $order = $orderService->checkOrderStatus($this->orderID);
        if(!$order['pass']){
            throw new OrderException([
                'msg' => '订单库存量不足',
            ]);
        }
        return $this->makeWxPreOrder($order['totalPrice']);
    }
    
    /* 校验订单是否有效 */
    public function checkOrderValid()
    {
        // 判断订单是否存在
        $order = OrderModel::find($this->orderID);
        if(!$order){
            throw new OrderException([
                'msg' => '订单不存在',
            ]);
        }
        // 判断订单是否属于当前用户
        if(!TokenService::checkUserOrder($order->user_id)){
            throw new OrderException([
                'msg' => '订单用户不符',
            ]);
        }
        // 判断订单状态是否为未支付
        if($order->status != OrderStatusEnum::UNPAID){
            throw new OrderException([
                'msg' => '订单状态有误',
            ]);
        }
        $this->orderNo = $order->order_no;
    }
    
    /* 创建预订单 */
    private function makeWxPreOrder($totalPrice)
    {
        // 获取openid
        $openid = TokenService::getCurrentTokenVal('openid');
        
        $wxOrderData = new \WxPayUnifiedOrder();
        // 设置订单号
        $wxOrderData->SetOut_trade_no($this->orderNo);
        // 设置支付类型
        $wxOrderData->SetTrade_type('JSAPI');
        // 设置项目描述
        $wxOrderData->SetBody('test');
        // 设置回调地址
        $wxOrderData->SetNotify_url(config('secure.pay_back_url'));
        // 设置金额
        $wxOrderData->SetTotal_fee($totalPrice*100);
        // 设置openid
        $wxOrderData->SetOpenid($openid);

        return $this->getSignature($wxOrderData);
    }

    /* 生成签名 */
    private function getSignature($wxOrderData)
    {
        $wxConfig = new \WxPayConfig();
        $wxOrder = \WxPayApi::unifiedOrder($wxConfig,$wxOrderData);
        // 判断创建预订单是否成功
        if($wxOrder['return_code']!='SUCCESS'){
            dump($wxOrder);
            return false;
        }
        $this->recordPreOrder($wxOrder);
        $signaTure = $this->sign($wxOrder,$wxConfig);
        return $signaTure;
    }

    /* 处理预订单 */
    private function recordPreOrder($wxOrder)
    {
        OrderModel::where('id',$this->orderID)->update(['prepay_id'=>$wxOrder['prepay_id']]);
    }

    /* 生成签名 */
    private function sign($wxOrder,$wxConfig)
    {
        $jsApiPayData = new \WxPayJsApiPay();
        $jsApiPayData->SetAppid(config('wx.app_id'));
        $jsApiPayData->SetTimeStamp((string)time());
        $rand = md5(time() . mt_rand(0,1000));
        $jsApiPayData->SetNonceStr($rand);
        $jsApiPayData->SetPackage('prepay_id='.$wxOrder['prepay_id']);
        $jsApiPayData->SetSignType('md5');
        $sign = $jsApiPayData->MakeSign($wxConfig);
        $rawValues = $jsApiPayData->GetValues();
        $rawValues['paySign'] = $sign;
        unset($rawValues['appId']);
        return $rawValues;
    }

}