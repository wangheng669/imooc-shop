<?php

namespace app\api\service;

use think\Loader;
use think\Db;
use think\Exception;
use app\api\model\Order as OrderModel;
use app\api\model\Product as ProductModel;
use app\api\service\Order as OrderService;
use app\lib\enum\OrderStatusEnum;

Loader::import('WxPay.WxPay',EXTEND_PATH,'.Api.php');

class WxNotify extends \WxPayNotify{

    /* 重写父类回调方法 */
    public function NotifyProcess($objData, $config, &$msg)
    {
        if($objData->values['return_code'] == 'SUCCESS'){
            $orderNo = $objData->values['out_trade_no'];
            Db::startTrans();
            try{
                $order = OrderModel::where('order_no',$orderNo)->find();
                if($order['status'] == 1){
                    $orderService = new OrderService();
                    $stockStatus = $orderService->checkOrderStatus($order['id']);
                    if($stockStatus['pass']){
                        // 更新订单状态
                        $this->updateOrderStatus($order->id,true);
                        // 更新库存
                        $this->reduceStock($stockStatus);
                    }else{
                        $this->updateOrderStatus($order->id,false);
                    }
                }
               Db::commit();
            }catch(Exception $e){
                Db::rollback();
                return false;
            }
            return true;
        }
    }

    /* 更新订单状态 */
    public function updateOrderStatus($orderID,$success)
    {
        $status = $success?OrderStatusEnum::PAID:OrderStatusEnum::PAID_BUT_OUT_OF;
        OrderModel::where('id',$orderID)->update(['status'=>2]);
    }

    /* 更新库存 */
    public function reduceStock($stockStatus)
    {
        foreach($stockStatus['pStatusArray'] as $signlePStatus){
            ProductModel::where('id',$signlePStatus['id'])->setDec('stock',$signlePStatus['count']);
        }
    }

}