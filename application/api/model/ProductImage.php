<?php

namespace app\api\model;

class ProductImage extends BaseModel
{

    protected $hidden = ['delete_time','img_id','product_id','id','order'];

    /* 关联image表 */
    public function img()
    {
        return $this->belongsTo('image','img_id','id');
    }
}
