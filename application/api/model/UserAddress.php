<?php

namespace app\api\model;

class UserAddress extends BaseModel
{
    public static function getUserAddressByUID($uid)
    {
        $userAddress = self::where('user_id',$uid)->find();
        return $userAddress;
    }
}
