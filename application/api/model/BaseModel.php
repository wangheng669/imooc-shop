<?php

namespace app\api\model;

use think\Model;

class BaseModel extends Model{

    public function prefixImageUrl($data,$value)
    {
        if($data['from'] == 1){
            $value = config('setting.image_url').$value;
        }
        return $value;
    }

}