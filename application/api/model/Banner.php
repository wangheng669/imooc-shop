<?php

namespace app\api\model;

class Banner extends BaseModel
{

    /* 隐藏字段 */
    protected $hidden = ['update_time','delete_time','id'];

    /* 关联banner_item */
    public function items()
    {
        return $this->hasMany('banner_item','banner_id','id');
    }

    /* 获取banner数据 */
    public static function getBannerByID($id)
    {
        $bannerList = self::with(['items','items.img'])->where('id','EQ',$id)->find();
        return $bannerList;
    }
}
