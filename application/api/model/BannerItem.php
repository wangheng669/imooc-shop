<?php

namespace app\api\model;

class BannerItem extends BaseModel
{

    /* 隐藏字段 */
    protected $hidden = ['img_id','delete_time','type','update_time','banner_id'];

    /* 关联image表 */
    public function img()
    {
        return $this->belongsTo('image','img_id','id');
    }
}
