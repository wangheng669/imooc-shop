<?php

namespace app\api\model;

use think\Paginator;

class Order extends BaseModel
{
    protected $autoWriteTimestamp = true;

    /* 地址转换格式 */
    public function getSnapItemsAttr($value)
    {
        return json_decode($value,true);
    }

    /* 地址转换格式 */
    public function getSnapAddressAttr($value)
    {
        return json_decode($value,true);
    }

    /* 获取订单详情 */
    public static function getOrderDetailByID($id)
    {
        $orderDetail = self::find($id);
        return $orderDetail;
    }

    /* 获取用户历史订单 */
    public static function getSummaryByUser($uid,$page,$size)
    {
        $pagingData = self::where('user_id',$uid)
        ->order('create_time desc')
        ->paginate(2,true,['page'=>$page]);
        return $pagingData;
    }
}
