<?php

namespace app\api\model;

class Image extends BaseModel
{
    /* 隐藏字段 */
    protected $hidden = ['delete_time','update_time','id','from'];

    public function getUrlAttr($data,$value)
    {
        return $this->prefixImageUrl($value,$data);
    }

}
