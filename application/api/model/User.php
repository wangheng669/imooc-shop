<?php

namespace app\api\model;

class User extends BaseModel
{
    
    /* 根据openid获取用户 */
    public static function getUserByOpenID($openid)
    {
        $user = self::where('openid','EQ',$openid)->find();
        return $user;
    }

    /* 根据uid获取用户 */
    public static function getUserByUID($uid)
    {
        $user = self::where('id','EQ',$uid)->with(['address'])->find();
        return $user;
    }

    /* 关联地址 address */
    public function address()
    {
        return $this->hasOne('userAddress','user_id','id');
    }

}
