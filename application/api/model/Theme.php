<?php

namespace app\api\model;

class Theme extends BaseModel
{

    protected $hidden = ['update_time','delete_time','topic_img_id','head_img_id'];

    /* 获取头图 */
    public function topicImg()
    {
        return $this->belongsTo('image','topic_img_id','id');
    }

    /* 获取首图 */
    public function headImg()
    {
        return $this->belongsTo('image','head_img_id','id');
    }

    /* 关联产品product 多对多 */
    public function products()
    {
        return $this->belongsToMany('product','theme_product','theme_id','product_id');
    }

    /* 获取专题列表 */
    public static function getThemeListByID($ids)
    {
        $idArray = explode(',',$ids);
        $themeList = self::with(['topicImg'])->select($idArray);
        return $themeList;
    }

    /* 获取专题详情 */
    public static function getThemeDetailByID($id)
    {
        $themeInfo = self::with(['products','topicImg'])->where('id','EQ',$id)->find();
        return $themeInfo;
    }
}
