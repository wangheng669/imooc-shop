<?php

namespace app\api\model;

class Category extends BaseModel
{

    /* 隐藏字段 */
    protected $hidden = ['update_time','delete_time','topic_img_id'];

    /* 获取头图 */
    public function topicImg()
    {
        return $this->belongsTo('image','topic_img_id','id');
    }

    /* 获取全部分类 */
    public static function getCategoryAll()
    {
        $categoryList = self::all([],['topicImg']);
        return $categoryList;
    }
}
