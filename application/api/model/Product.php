<?php

namespace app\api\model;

class Product extends BaseModel
{
    protected $hidden = ['pivot','from','delete_time','category_id','create_time','update_time','summary','img_id'];

    /* 关联商品参数表property */
    public function properties()
    {
        return $this->hasMany('product_property','product_id','id');
    }

    /* 关联商品表 */
    public function imgs()
    {
        return $this->hasMany('product_image','product_id','id');
    }

    /* 添加图片前缀 */
    public function getMainImgUrlAttr($data,$value)
    {
        return $this->prefixImageUrl($value,$data);
    }

    /* 获取最近商品 */
    public static function getProductsByCount($count)
    {
        $productList = self::limit($count)->order('id','desc')->select();
        return $productList;
    }

    /* 获取商品详情 */
    public static function getProductDetail($id)
    {
        $productInfo = self::with(['imgs'=>function($query){
            $query->with(['img'])->order('order','asc');
        }])->with(['properties'])->where('id','EQ',$id)->find();
        return $productInfo;
    }

    /* 根据分类获取商品列表 */
    public static function getProductsByCategoryID($id)
    {
        $productList = self::where('category_id','EQ',$id)->select();
        return $productList;
    }

}
