<?php

namespace app\api\controller;

use think\Controller;
use app\api\service\Token as TokenService;

class BaseController extends Controller{

    public function checkPrimayScope()
    {
        TokenService::needPrimayScope();   
    }

    public function checkExclusiveScope()
    {
        TokenService::needExclusiveScope();   
    }

}