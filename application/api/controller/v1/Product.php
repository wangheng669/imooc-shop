<?php

namespace app\api\controller\v1;

use app\api\controller\BaseController;
use app\api\validate\Count;
use app\api\validate\IDMustBePositiveInt;
use app\lib\exception\ProductException;
use app\api\model\Product as ProductModel;

class Product extends BaseController
{
    /**
     * 
     * @api {GET} /procduct/count/:id 获取最近商品 
     * @apiName getRecentProduct
     * @apiGroup Product
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {Number} count 商品数量
     * 
     * @apiSuccess (200) {Object} productList 最近商品列表
     * 
     */
    public function getRecentProduct($count = 15)
    {
        (new Count())->goCheck();
        $productList = ProductModel::getProductsByCount($count);
        if($productList->isEmpty()){
            throw new ProductException();
        }
        return $productList;
    }

    /**
     * 
     * @api {POST} /product/:id 获取商品详情
     * @apiName getProductDetail
     * @apiGroup Product
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {Number} id 商品id
     * 
     * @apiSuccess (200) {Object} prodcutInfo 商品详情信息
     * 
     */
    public function getProductDetail($id)
    {
        (new IDMustBePositiveInt())->goCheck();
        $productInfo = ProductModel::getProductDetail($id);
        if(!$productInfo){
            throw new ProductException();
        }
        return $productInfo;
    }

    /**
     * 
     * @api {GET} /product/category/:id 根据分类获取商品
     * @apiName getProductInCategory
     * @apiGroup Product
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {Number} id 分类id
     * 
     * @apiSuccess (200) {Object} productList 分类下的商品列表
     * 
     */

     public function getProductInCategory($id)
     {
        (new IDMustBePositiveInt())->goCheck();
        $productList = productModel::getProductsByCategoryID($id);
        if($productList->isEmpty()){
            throw new ProductException();
        }
        return $productList;
     }

}
