<?php

namespace app\api\controller\v1;

use app\api\controller\BaseController;
use app\api\service\UserToken as UserToken;
use app\api\validate\TokenGet;


class Token extends BaseController
{

    /**
     * 
     * @api {POST} /token/user 获取token令牌
     * @apiName getToken
     * @apiGroup Token
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {Number} code 微信登录code码
     * 
     * @apiSuccess (200) {Object} token token令牌 
     * 
     */
    public function getToken($code='')
    {
        (new TokenGet())->goCheck();
        $ut = new UserToken($code);
        $token = $ut->get();
        return [
            'token' => $token,
        ];
    }

}
