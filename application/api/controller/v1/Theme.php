<?php

namespace app\api\controller\v1;

use app\api\controller\BaseController;
use app\api\validate\IDCollection;
use app\api\validate\IDMustBePositiveInt;
use app\lib\exception\ThemeException;
use app\api\model\Theme as ThemeModel;

class Theme extends BaseController
{

    /**
     * 
     * @api {GET} /theme?ids 专题列表
     * @apiName getThemeList
     * @apiGroup Theme
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {String} ids id集合
     * 
     * @apiSuccess (200) {Object} themeList theme列表信息
     * 
     */
    public function getThemeList($ids='')
    {
        /* 校验ID集合是否为正整数 */
        (new IDCollection())->goCheck();
        $themeList = ThemeModel::getThemeListByID($ids);
        if($themeList->isEmpty()){
            throw new ThemeException();
        }
        return $themeList;
    }

    /**
     * 
     * @api {GET} /theme/:id 专题详情
     * @apiName getThemeDetail
     * @apiGroup Theme
     * @apiVersion  1.0.0
     * 
     * 
     * @apiParam  {Number} id 专题id
     * 
     * @apiSuccess (200) {Object} themeInfo 专题详情信息
     * 
     */
    public function getThemeDetail($id)
    {
        (new IDMustBePositiveInt())->goCheck();
        $themeInfo = ThemeModel::getThemeDetailByID($id);
        if(!$themeInfo){
            throw new ThemeException();
        }
        return $themeInfo;
    }
    

}
