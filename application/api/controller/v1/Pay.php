<?php

namespace app\api\controller\v1;

use app\api\controller\BaseController;
use app\api\service\Pay as PayService;
use app\api\validate\IDMustBePositiveInt;
use app\api\service\WxNotify;
use think\Loader;

Loader::import('WxPay.WxPay',EXTEND_PATH,'.Api.php');

class Pay extends BaseController
{

     /* 前置校验权限 */
    protected $beforeActionList = [
        'checkExclusiveScope' => ['only' => 'createorupdate']
    ];


    /* 统一支付 */
    public function preOrder($id)
    {
        (new IDMustBePositiveInt())->goCheck();
        $pay = new PayService($id);
        return $pay->pay();
    }

    /* 回调处理 */
    public function notify()
    {
        $wxConfig = new \WxPayConfig();
        $wxNotify = new WxNotify();
        $wxNotify->Handle($wxConfig);
    }

    /* 回调测试方法 */
    public function receiveNotify()
    {
       $xmlData = file_get_contents('php://input');
       curl_post_raw('http:/www.zerg.com/pay/notify?XDEBUG_SESSION_START=13133',
           $xmlData);
    }

}

