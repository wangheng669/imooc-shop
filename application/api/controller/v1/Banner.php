<?php

namespace app\api\controller\v1;

use app\api\controller\BaseController;
use app\api\validate\IDMustBePositiveInt;
use app\lib\exception\BannerException;
use app\api\model\Banner as BannerModel;

class Banner extends BaseController
{
    
    /**
     * 
     * @api {GET} /banner/:id 轮播信息
     * @apiName getBanner
     * @apiGroup Banner
     * @apiVersion  1.0.0
     * 
     * @apiParam  {Number} id banner ID
     * @apiSuccess (200) {Object} banner信息
     * 
     */
    public function getBannerList($id)
    {
        /* 参数校验 */
        (new IDMustBePositiveInt())->goCheck();
        $bannerList = BannerModel::getBannerByID($id);
        if(!$bannerList){
            throw new BannerException();
        }
        return $bannerList;
    }
}
