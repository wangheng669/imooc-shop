<?php

namespace app\api\controller\v1;

use app\api\controller\BaseController;
use app\api\model\Category as CategoryModel;
use app\lib\exception\CategoryException;

class Category extends BaseController
{

    /**
     * 
     * @api {GET} /category 获取全部分类列表
     * @apiName getCategoryAll
     * @apiGroup Category
     * @apiVersion  1.0.0
     * 
     * 
     * @apiSuccess (200) {Object} categoryList 全部分类列表
     * 
     */
    public function getCategoryAll()
    {
        $categoryList = CategoryModel::getCategoryAll();
        if(!$categoryList){
            throw new CategoryException();
        }
        return $categoryList;
    }

}
