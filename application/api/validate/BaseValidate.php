<?php

namespace app\api\validate;

use think\Validate;
use app\lib\exception\ParamterException;

class BaseValidate extends Validate{

    /* 公共校验方法 */
    public function goCheck()
    {
        $data = request()->param();
        $result = $this->batch(true)->check($data);
        if(!$result){
            throw new ParamterException([
                'msg' => $this->getError(),
            ]);
        }
    }

    /* 校验正整数 */
    public function isPositiveInteger($value)
    {
        if(is_numeric($value)&&is_int($value+0)&&($value+0)>0){
            return true;
        }
        return false;
    }

    /* 判断不能为空 */
    public function isEmpty($value)
    {
        if(empty($value)){
            return false;
        }
        return true;
    }

    /* 判断手机号 */
    public function isMobile($value)
    {
        if(!preg_match('^1(3|4|5|7|8)[0-9]\d{8}$^',$value)){
            return false;
        }
        return true;
    }

}