<?php

namespace app\api\validate;

use app\lib\exception\ParamterException;

class OrderPlace extends BaseValidate{

    protected $rule = [
        'products' => 'require|checkProducts'
    ];

    protected $signleRule = [
        'product_id' => 'require|isPositiveInteger',
        'count' => 'require|isPositiveInteger',
    ];

    /* 校验商品数组 */
    public function checkProducts($products)
    {
        if(!is_array($products)||empty($products)){
            return false;
        }
        foreach($products as $product){
            $this->checkProduct($product);
        }
        return true;
    }

    /* 校验单一商品 */
    public function checkProduct($product)
    {
        $validate = new BaseValidate($this->signleRule);
        if(!$validate->check($product)){
            throw new ParamterException();
        }
    }

}