<?php

namespace app\api\validate;

class IDCollection extends BaseValidate{

    protected $rule = [
        'ids' => 'require|checkIDs',
    ];

    /* 校验ID集合是否为数组 */
    public function checkIDs($ids)
    {
        $idArray = explode(',',$ids);
        if(!is_array($idArray)||empty($ids)){
            return false;
        }
        /* 通过foreach遍历数组元素判断是否为正整数 */
        foreach($idArray as $id){
            if(!$this->isPositiveInteger($id)){
                return false;
            }
        }
        return true;
    }

}