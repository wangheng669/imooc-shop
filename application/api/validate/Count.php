<?php

namespace app\api\validate;

class Count extends BaseValidate{

    protected $rule = [
        'count' => 'between:1,15'
    ];

}