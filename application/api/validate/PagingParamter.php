<?php

namespace app\api\validate;

class PagingParamter extends BaseValidate{

    protected $rule = [
        'page' => 'isPositiveInteger',
        'size' => 'isPositiveInteger',
    ];

}