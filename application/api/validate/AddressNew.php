<?php

namespace app\api\validate;

use app\lib\exception\ParamterException;

class AddressNew extends BaseValidate{

    protected $rule = [
        'city' => 'require|isEmpty',
        'province' => 'require|isEmpty',
        'country' => 'require|isEmpty',
        'detail' => 'require|isEmpty',
        'mobile' => 'require|isEmpty',
    ];

    /* 校验参数是否有效 */
    public function checkDataVaild($value)
    {
        if(array_key_exists('user_id',$value) || array_key_exists('uid',$value)){
            throw new ParamterException();
        }
        $newArr = [];
        foreach($this->rule as $k=>$v){
            $newArr[$k] = $value[$k];
        }
        return $newArr;
    }

}