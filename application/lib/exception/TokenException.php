<?php

namespace app\lib\exception;

class TokenException extends BaseException{

    public $msg = 'token获取失败';

    public $code = 404;
    
    public $errCode = 50000;

}