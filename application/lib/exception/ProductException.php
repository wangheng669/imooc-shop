<?php

namespace app\lib\exception;

class ProductException extends BaseException{

    public $msg = '商品不存在';

    public $code = 404;
    
    public $errCode = 30000;

}