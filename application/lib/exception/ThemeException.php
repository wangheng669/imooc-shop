<?php

namespace app\lib\exception;

class ThemeException extends BaseException{

    public $msg = '专题不存在';

    public $code = 404;
    
    public $errCode = 30000;

}