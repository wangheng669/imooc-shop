<?php

namespace app\lib\exception;

class BannerException extends BaseException{

    public $msg = 'banner不存在';

    public $code = 404;
    
    public $errCode = 20000;

}