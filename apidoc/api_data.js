define({ "api": [
  {
    "type": "POST",
    "url": "/address",
    "title": "新增或更新地址",
    "name": "createOrUpdate",
    "group": "Address",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "mobile",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "province",
            "description": "<p>省</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>市</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>区</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "detail",
            "description": "<p>详细地址</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "success",
            "description": "<p>写入状态</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/v1/Address.php",
    "groupTitle": "Address"
  },
  {
    "type": "GET",
    "url": "/banner/:id",
    "title": "轮播信息",
    "name": "getBanner",
    "group": "Banner",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>banner ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "banner",
            "description": "<p>信息</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/v1/Banner.php",
    "groupTitle": "Banner"
  },
  {
    "type": "GET",
    "url": "/category",
    "title": "获取全部分类列表",
    "name": "getCategoryAll",
    "group": "Category",
    "version": "1.0.0",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "categoryList",
            "description": "<p>全部分类列表</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/v1/Category.php",
    "groupTitle": "Category"
  },
  {
    "type": "POST",
    "url": "/product/:id",
    "title": "获取商品详情",
    "name": "getProductDetail",
    "group": "Product",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>商品id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "prodcutInfo",
            "description": "<p>商品详情信息</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/v1/Product.php",
    "groupTitle": "Product"
  },
  {
    "type": "GET",
    "url": "/product/category/:id",
    "title": "根据分类获取商品",
    "name": "getProductInCategory",
    "group": "Product",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>分类id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "productList",
            "description": "<p>分类下的商品列表</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/v1/Product.php",
    "groupTitle": "Product"
  },
  {
    "type": "GET",
    "url": "/procduct/count/:id",
    "title": "获取最近商品",
    "name": "getRecentProduct",
    "group": "Product",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>商品数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "productList",
            "description": "<p>最近商品列表</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/v1/Product.php",
    "groupTitle": "Product"
  },
  {
    "type": "GET",
    "url": "/theme/:id",
    "title": "专题详情",
    "name": "getThemeDetail",
    "group": "Theme",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>专题id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "themeInfo",
            "description": "<p>专题详情信息</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/v1/Theme.php",
    "groupTitle": "Theme"
  },
  {
    "type": "GET",
    "url": "/theme?ids",
    "title": "专题列表",
    "name": "getThemeList",
    "group": "Theme",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ids",
            "description": "<p>id集合</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "themeList",
            "description": "<p>theme列表信息</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/v1/Theme.php",
    "groupTitle": "Theme"
  },
  {
    "type": "POST",
    "url": "/token/user",
    "title": "获取token令牌",
    "name": "getToken",
    "group": "Token",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>微信登录code码</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "token",
            "description": "<p>token令牌</p>"
          }
        ]
      }
    },
    "filename": "application/api/controller/v1/Token.php",
    "groupTitle": "Token"
  }
] });
